# Manejo de archivos con la declaración `with` en python.

Esta declaración se utiliza especialmente para el manejo de excepciones y para que el código
sea mucho mas limpio en consecuencia
legible.

#### Para utilizar la declaración `with` solo se necesita agregar los siguientes métodos


* `__enter__`: se encarga principalmente utilizar la función `open()` ya sea para escribir o leer un texto en especifico.

* `__exit__` : se encarga de cerrar el archivo de texto, incluyendo las excepciones del proceso.


## Ejemplo:

~~~
with open("file_path", "w", encoding="utf8") as file:
    file.write("hola mundo!")
~~~
